name = "cmake"

version = "3.24.2"

variants = [
    ["platform-windows", "arch-AMD64"],
]

requires = [
    "msvc",
]

build_command = ""

with scope("config") as c:
    c.release_packages_path = r"D:\rez\pkgs\ext"


def commands():
    env.PATH.append(this._cmake_dir)


@early()
def _cmake_dir():
    import os

    cmake_dir = os.path.join(os.getenv("PROGRAMFILES"), "CMake", str(this.version), "bin")
    return cmake_dir
